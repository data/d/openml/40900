# OpenML dataset: Satellite

https://www.openml.org/d/40900

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Markus Goldstein  
**Source**: [Dataverse](http://www.madm.eu/downloads https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/OPQMVF)  
**Please cite**:   

The satellite dataset comprises of features extracted from satellite observations. In particular, each image was taken under four different light wavelength, two in visible light (green and red) and two infrared images. The task of the original dataset is to classify the image into the soil category of the observed region. 

### Classes
We defined the soil classes &ldquo;red soil&rdquo;, &ldquo;gray soil&rdquo;, &ldquo;damp gray soil&rdquo; and &ldquo;very damp gray soil&rdquo; as the normal class. From the semantically different classes &ldquo;cotton crop&rdquo; and &ldquo;soil with vegetation stubble&rdquo; anomalies are sampled. 

After merging the original training and test set into a single dataset, the resulting dataset contains 5,025 normal instances as well as 75 randomly sampled anomalies (1.49%) with 36 dimensions 

### Relevant Papers

Goldstein, Markus, and Seiichi Uchida. A comparative evaluation of unsupervised anomaly detection algorithms for multivariate data.&quot; PloS one 11.4 (2016): e0152173 

This dataset is not the original dataset. The target variable 'Target' is relabeled into 'Normal' and 'Anomaly'

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40900) of an [OpenML dataset](https://www.openml.org/d/40900). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40900/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40900/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40900/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

